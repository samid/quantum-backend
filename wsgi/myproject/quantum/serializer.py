from rest_framework import serializers
from .models import Node


class NodeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Node
        fields = ('id', 'parent', 'value', 'deleted')


class DictNodeSerializer(serializers.Serializer):
    node = NodeSerializer()
    id = serializers.CharField(max_length=255)

    class Meta:
        model = Node
        fields = ('parent', 'value', 'deleted')
