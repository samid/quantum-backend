from django.contrib import admin
from .models import Node


class NodeAdmin(admin.ModelAdmin):
    list_display = ('id', 'parent', 'value', 'deleted')


admin.site.register(Node, NodeAdmin)
