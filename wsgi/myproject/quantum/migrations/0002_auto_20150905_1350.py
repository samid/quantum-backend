# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('quantum', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='node',
            name='parent',
            field=models.IntegerField(blank=True, null=True),
        ),
    ]
