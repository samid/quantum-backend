# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Node',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True)),
                ('value', models.CharField(max_length=255)),
                ('deleted', models.BooleanField(default=False)),
                ('parent', models.ForeignKey(null=True, related_name='children', to='quantum.Node', blank=True)),
            ],
        ),
    ]
