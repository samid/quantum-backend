from django.test import TestCase
from .models import Node
from .models import NodeManager

class NodeTestCase(TestCase):
    def setUp(self):
        NodeManager.reset()

    def test_animals_can_speak(self):
        """Animals that can speak are correctly identified"""
        lion = Animal.objects.get(name="lion")
        cat = Animal.objects.get(name="cat")
        self.assertEqual(lion.speak(), 'The lion says "roar"')
        self.assertEqual(cat.speak(), 'The cat says "meow"')