from django.http import HttpResponse
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status
from .models import Node
from .models import NodeManager
from .serializer import NodeSerializer
import json


@api_view(['GET'])
def nodes(request):
    nodes_dict = NodeManager.get_dict()
    for key in nodes_dict:
        nodes_dict[key] = NodeSerializer(nodes_dict[key]).data
    return Response(data=nodes_dict)


@api_view(['GET'])
def node(request, node_id):
    if not node_id:
        node_id = request.GET.get('node_id', None)
    try:
        node_item = Node.objects.get(id=node_id)
    except Node.DoesNotExist:
        return HttpResponse(status=status.HTTP_404_NOT_FOUND)
    serializer = NodeSerializer(node_item)
    return Response(data=serializer.data)


@api_view(['GET', 'POST'])
def reset(request):
    NodeManager.reset()
    nodes_dict = NodeManager.get_dict()
    for key in nodes_dict:
        nodes_dict[key] = NodeSerializer(nodes_dict[key]).data
    return Response(data=nodes_dict)


@api_view(['POST'])
def apply(request):
    created_data = request.data.get('created', {})
    modified_data = request.data.get('modified', {})
    deleted_data = request.data.get('deleted', {})
    not_changed_data = request.data.get('notChanged', {})

    modified_nodes = NodeManager.apply_changes(created_data,
                                               modified_data,
                                               deleted_data,
                                               not_changed_data)

    for key in modified_nodes:
        modified_nodes[key] = NodeSerializer(modified_nodes[key]).data

    return Response(data=modified_nodes)
