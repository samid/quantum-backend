from django.db import models


class NodeManager(models.Manager):
    @staticmethod
    def __create_child_node(parent, level, max_level):
        latest_id = Node.objects.count() + 1
        value = 'Node ' + str(latest_id)
        n = Node(value=value, parent=parent)
        n.save()
        if level < max_level:
            NodeManager.__create_child_node(n.id, level + 1, max_level)
            NodeManager.__create_child_node(n.id, level + 1, max_level)

    @staticmethod
    def __create_tree():
        NodeManager.__create_child_node(None, 0, 4)

    @staticmethod
    def __delete_children(parent_id, modified_nodes, old_nodes_ids_map, modified, not_changed):
        child_nodes = Node.objects.filter(parent=parent_id)
        for child in child_nodes:
            child.deleted = True
            child.save()

            if str(child.id) in old_nodes_ids_map and old_nodes_ids_map[str(child.id)] in modified_nodes:
                modified_nodes[old_nodes_ids_map[str(child.id)]].deleted = True
            elif str(child.id) in modified or str(child.id) in not_changed:
                modified_nodes[str(child.id)] = child

            NodeManager.__delete_children(child.id, modified_nodes, old_nodes_ids_map, modified, not_changed)

    @staticmethod
    def reset():
        Node.objects.all().delete()
        NodeManager.__create_tree()

    @staticmethod
    def get_dict():
        id_list = Node.objects.values_list('id', flat=True)
        model_dict = Node.objects.in_bulk(id_list)
        return model_dict

    @staticmethod
    def apply_changes(created, modified, deleted, not_changed):
        new_nodes_ids_map = {}
        old_nodes_ids_map = {}
        modified_nodes = {}

        if len(created) > 0:
            for key in created:
                node = Node(value=created[key]['value'], parent=str(created[key]['parent']))
                node.save()
                new_nodes_ids_map[str(key)] = node.id
                old_nodes_ids_map[str(node.id)] = key
                modified_nodes[key] = node

            need_to_update = Node.objects.filter(parent__contains='new')

            for node in need_to_update:
                if node.parent in new_nodes_ids_map:
                    old_id = node.parent
                    node.parent = str(new_nodes_ids_map[old_id])
                    node.save()
                    modified_nodes[old_nodes_ids_map[str(node.id)]] = node
                    print('change old parent id=' + old_id + ' to new id=' + node.parent)
                else:
                    print('cant find new id for parent=' + node.parent)

        if len(modified) > 0:
            for key in modified:
                node = Node.objects.get(id=key)
                node.value = modified[key]['value']
                node.save()

        if len(deleted) > 0:
            for key in deleted:
                node = Node.objects.get(id=key)
                node.deleted = True
                node.save()
                print('before __delete_children call', modified_nodes)
                NodeManager.__delete_children(key, modified_nodes, old_nodes_ids_map, modified, not_changed)
                print('after __delete_children call', modified_nodes)

        return modified_nodes


class Node(models.Model):
    id = models.AutoField(primary_key=True)
    parent = models.CharField(null=True, blank=True, max_length=255)
    value = models.CharField(max_length=255)
    deleted = models.BooleanField(default=False)

    def __unicode__(self):
        return self.value

    def __str__(self):
        return self.value
