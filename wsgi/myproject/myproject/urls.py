from django.conf.urls import patterns, include, url
from django.contrib import admin
from quantum import views

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'myproject.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^api/nodes', views.nodes),
    url(r'^api/node/(?P<node_id>[-\w]+)', views.node),
    url(r'^api/apply', views.apply),
    url(r'^api/reset', views.reset),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
)
